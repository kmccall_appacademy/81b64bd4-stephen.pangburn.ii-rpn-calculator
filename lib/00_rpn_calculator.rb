# rpn_calculator.rb

class RPNCalculator

  def initialize
    @nums = []
  end

  def push(num)
    @nums << num
  end

  def value
    @nums.last
  end

  def plus
    do_operation(:+)
  end

  def minus
    do_operation(:-)
  end

  def times
    do_operation(:*)
  end

  def divide
    do_operation(:/)
  end

  def tokens(string)
    operations = []
    string.split.each do |char|
      case char
      when "+"
        operations << :+
      when "-"
        operations << :-
      when "*"
        operations << :*
      when "/"
        operations << :/
      else
        operations << char.to_f
      end
    end
    operations
  end

  def evaluate(string)
    sequence = tokens(string)
    @nums = []

    sequence.each do |token|
      case token
      when :+
        self.plus
      when :-
        self.minus
      when :*
        self.times
      when :/
        self.divide
      else
        @nums << token
      end
    end
    value
  end

  private

  def do_operation(sym)
    raise "calculator is empty" if @nums.empty?
    second = @nums.pop
    first = @nums.pop

    case sym
    when :+
      @nums << first + second
    when :-
      @nums << first - second
    when :*
      @nums << first * second
    when :/
      @nums << first.to_f / second.to_f
    else
      raise "not a valid operation: #{sym}"
    end
    value
  end
end
